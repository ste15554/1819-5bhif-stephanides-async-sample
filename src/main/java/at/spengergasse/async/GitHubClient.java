package at.spengergasse.async;

import at.spengergasse.async.model.Repository;
import at.spengergasse.async.model.User;
import at.spengergasse.async.model.commit.Commit;
import at.spengergasse.async.rest.GitHubRestClient;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class GitHubClient {

    private GitHubRestClient client;

    public GitHubClient() {
        this.client = Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(GitHubRestClient.class))
                .logLevel(Logger.Level.FULL)
                .target(GitHubRestClient.class, "https://api.github.com/");
    }

    public Map<String, String> getEndpoints() {
        return this.client.getEndpoints();
    }

    public CompletableFuture<Map<String, String>> getEndpointsAsync() {
        return CompletableFuture.supplyAsync(this::getEndpoints);
    }

    public User getUser(String user) {
        return this.client.getUser(user);
    }

    public CompletableFuture<User> getUserAsync(String user) {
        return CompletableFuture.supplyAsync(() -> this.getUser(user));
    }

    public Repository getRepository(String owner, String repository) {
        return this.client.getRepository(owner, repository);
    }

    public CompletableFuture<Repository> getRepositoryAsync(String owner, String repository) {
        return CompletableFuture.supplyAsync(() -> this.getRepository(owner, repository));
    }

    public List<Commit> getRepositoryCommits(String owner, String repository) {
        return this.client.getRepositoryCommits(owner, repository);
    }

    public CompletableFuture<List<Commit>> getRepositoryCommitsAsync(String owner, String repository) {
        return CompletableFuture.supplyAsync(() -> this.getRepositoryCommits(owner, repository));
    }
}

package at.spengergasse.async;

import at.spengergasse.async.model.commit.Commit;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SampleApplication {

    private GitHubClient client;

    public SampleApplication() {
        this.client = new GitHubClient();
        this.readCommitsSequentially();
        this.readCommitsParallel();
    }

    public void readCommitsSequentially() {
        long startTime = System.currentTimeMillis();
        System.out.println("--- Starting SEQUENTIALLY sample  ---");

        List<Commit> commits1 = this.client.getRepositoryCommits("markusstephanides", "spengerclub-frontend");
        List<Commit> commits2 = this.client.getRepositoryCommits("markusstephanides", "Sharpit");
        List<Commit> commits3 = this.client.getRepositoryCommits("markusstephanides", "janda");
        List<Commit> commits4 = this.client.getRepositoryCommits("markusstephanides", "DSP");

        System.out.println("spengerclub-frontend has " + commits1.size() + " commits");
        System.out.println("Sharpit has " + commits2.size() + " commits");
        System.out.println("janda has " + commits3.size() + " commits");
        System.out.println("DSP has " + commits4.size() + " commits");
        System.out.println("Sequentially took " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public void readCommitsParallel() {
        long startTime = System.currentTimeMillis();
        System.out.println("--- Starting PARALLEL sample  ---");

        CompletableFuture<List<Commit>> future1 = this.client.getRepositoryCommitsAsync("markusstephanides", "spengerclub-frontend");
        CompletableFuture<List<Commit>> future2 = this.client.getRepositoryCommitsAsync("markusstephanides", "Sharpit");
        CompletableFuture<List<Commit>> future3 = this.client.getRepositoryCommitsAsync("markusstephanides", "janda");
        CompletableFuture<List<Commit>> future4 = this.client.getRepositoryCommitsAsync("markusstephanides", "DSP");

        CompletableFuture allFutures = CompletableFuture.allOf(
                future1, future2, future3, future4
        );

        try {
            allFutures.get();

            List<Commit> commits1 = future1.get();
            List<Commit> commits2 = future2.get();
            List<Commit> commits3 = future3.get();
            List<Commit> commits4 = future4.get();

            System.out.println("spengerclub-frontend has " + commits1.size() + " commits");
            System.out.println("Sharpit has " + commits2.size() + " commits");
            System.out.println("janda has " + commits3.size() + " commits");
            System.out.println("DSP has " + commits4.size() + " commits");

            System.out.println("Parallel took " + (System.currentTimeMillis() - startTime) + " ms");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }
}

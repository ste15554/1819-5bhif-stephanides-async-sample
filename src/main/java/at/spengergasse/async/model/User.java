package at.spengergasse.async.model;

import lombok.Data;

@Data
public final class User {

    private String login;
    private Integer id;
    private String nodeId;
    private String avatarUrl;
    private String gravatarId;
    private String url;
    private String htmlUrl;
    private String followersUrl;
    private String followingUrl;
    private String gistsUrl;
    private String starredUrl;
    private String subscriptionsUrl;
    private String organizationsUrl;
    private String reposUrl;
    private String eventsUrl;
    private String receivedEventsUrl;
    private String type;
    private Boolean siteAdmin;
    private String name;
    private Object company;
    private String blog;
    private Object location;
    private Object email;
    private Boolean hireable;
    private Object bio;
    private Integer privateRepos;
    private Integer privateGists;
    private Integer followers;
    private Integer following;
    private String createdAt;
    private String updatedAt;

}

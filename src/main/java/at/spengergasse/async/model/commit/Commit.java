package at.spengergasse.async.model.commit;

import at.spengergasse.async.model.User;
import lombok.Data;

import java.util.List;

@Data
public class Commit {

    private String sha;
    private String nodeId;
    private CommitData commit;
    private String url;
    private String htmlUrl;
    private String commentsUrl;
    private User author;
    private User committer;
    private List<CommitParent> parents;

}

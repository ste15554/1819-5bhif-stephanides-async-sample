package at.spengergasse.async.model.commit;

public class CommitData {

    private CommitUser author;
    private CommitUser committer;
    private String message;
    private CommitTree tree;
    private String url;
    private int comment_count;
    private CommitVerification verification;

}

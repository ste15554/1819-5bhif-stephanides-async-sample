package at.spengergasse.async.model.commit;

import lombok.Data;

@Data
public class CommitParent {

    private String sha;
    private String url;
    private String htmlUrl;

}

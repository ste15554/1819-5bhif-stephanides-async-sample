package at.spengergasse.async.model.commit;

import lombok.Data;

@Data
public class CommitTree {

    private String sha;
    private String url;

}

package at.spengergasse.async.model.commit;

import lombok.Data;

@Data
public class CommitUser {

    private String name;
    private String email;
    private String date;

}

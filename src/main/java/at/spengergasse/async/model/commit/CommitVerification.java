package at.spengergasse.async.model.commit;

import lombok.Data;

@Data
public class CommitVerification {

    private boolean verified;
    private String reason;
    private String signature;
    private String payload;

}

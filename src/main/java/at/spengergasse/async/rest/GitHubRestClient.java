package at.spengergasse.async.rest;

import at.spengergasse.async.model.Repository;
import at.spengergasse.async.model.User;
import at.spengergasse.async.model.commit.Commit;
import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

public interface GitHubRestClient {

    @RequestLine("GET /")
    Map<String, String> getEndpoints();

    @RequestLine("GET /users/{user}")
    User getUser(@Param("user") String user);

    @RequestLine("GET /repos/{owner}/{repo}")
    Repository getRepository(@Param("owner") String owner, @Param("repo") String repo);

    @RequestLine("GET /repos/{owner}/{repo}/commits")
    List<Commit> getRepositoryCommits(@Param("owner") String owner, @Param("repo") String repo);

}

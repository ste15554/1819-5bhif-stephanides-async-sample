package at.spengergasse.async;

import at.spengergasse.async.model.Repository;
import at.spengergasse.async.model.User;
import at.spengergasse.async.model.commit.Commit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AsyncApplicationTests {

    private GitHubClient client;

    @Before
    public void setUp() {
        this.client = new GitHubClient();
    }

    @Test
    public void testGetEndpointsSync() {
        Map<String, String> endpoints = this.client.getEndpoints();

        // as of march 20th, there are 31 endpoints existent
        assertEquals(31, endpoints.size());
    }

    @Test
    public void testGetEndpointsAsync() throws Exception {
        CompletableFuture<Map<String, String>> future = this.client.getEndpointsAsync();

        // as of march 20th, there are 31 endpoints existent
        assertEquals(31, future.get().size());
    }

    @Test
    public void testGetUserSync() {
        String testUsername = "markusstephanides";

        User user = this.client.getUser(testUsername);
        assertEquals(testUsername, user.getLogin());
    }

    @Test
    public void testGetUserAsync() throws Exception {
        String testUsername = "markusstephanides";

        CompletableFuture<User> future = this.client.getUserAsync(testUsername);

        assertEquals(testUsername, future.get().getLogin());
    }

    @Test
    public void testGetRepoSync() {
        String testOwner = "markusstephanides";
        String testRepository = "spengerclub-frontend";

        Repository repository = this.client.getRepository(testOwner, testRepository);
        assertEquals(testRepository, repository.getName());
    }

    @Test
    public void testGetRepoAsync() throws Exception {
        String testOwner = "markusstephanides";
        String testRepository = "spengerclub-frontend";

        CompletableFuture<Repository> future = this.client.getRepositoryAsync(testOwner, testRepository);

        assertEquals(testRepository, future.get().getName());
        assertEquals(testOwner, future.get().getOwner().getLogin());
    }

    @Test
    public void testGetCommitsSync() {
        String testOwner = "markusstephanides";
        String testRepository = "spengerclub-frontend";

        List<Commit> commits = this.client.getRepositoryCommits(testOwner, testRepository);
        assertEquals(19, commits.size()); // 19 commits as of march 20th
    }

    @Test
    public void testGetCommitsAsync() throws Exception {
        String testOwner = "markusstephanides";
        String testRepository = "spengerclub-frontend";

        CompletableFuture<List<Commit>> future = this.client.getRepositoryCommitsAsync(testOwner, testRepository);

        assertEquals(19, future.get().size()); // 19 commits as of march 20th
    }

}
